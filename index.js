var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
const cors = require('cors');
 
var app = express();
const port = 3000;
 
// Importamos las rutas
var nota_routes = require('./Rutas/rutas');
 
// body-parser
app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ limit: '10mb' }))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'))
app.use(cors())
// Cargamos las rutas

app.use('', nota_routes);
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`)) 
