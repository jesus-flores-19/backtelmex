var mysql = require('promise-mysql');
const { database } = require('./keys');

var pool = mysql.createPool(database);

pool.getConnection((err, connection) => {
    if (err) { console.log("Error") }
    if (connection) connection.release();
    console.log("BD conectada")
})

module.exports = pool;