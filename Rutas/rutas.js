// Cargamos el módulo de express para poder crear rutas
var express = require('express');
 
// Cargamos el controlador
var NotaController = require('../Controller/controlador');
 
// Llamamos al router
var api = express.Router();
 
// Creamos una ruta de tipo GET para el método de pruebas
api.get('/agregar/Viva/IDE', NotaController.AgregarVivaIDE);
api.get('/agregar/Viva/Infinitum', NotaController.AgregarVivaInfinitum);
api.get('/obtener/productos/IDE', NotaController.ObtenerProductosIDE)
api.get('/obtener/productos/Infinitum', NotaController.ObtenerProductosInfinitum)
api.get('/obtener/familias/IDE', NotaController.ObtenerFamiliasIDE);
api.get('/obtener/familias/Infinitum', NotaController.ObtenerFamiliasInfinitum);

api.get('/obtener/')
api.post('/crear/documento', NotaController.CrearDocumento);


 
// Exportamos la configuración
module.exports = api;
