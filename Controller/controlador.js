var fs =  require('fs');
const path = require('path');
var mysql      = require('mysql');
const { O_WRONLY } = require('constants');
const { get } = require('http');
const { rejects } = require('assert');
var connectionDB = mysql.createPool({
    host     : 'elipgomexico.ddns.net',
    // host: '10.200.2.101', //Direccion IP
    user     : 'crmdb',
    password : 'crmDB-2019',
    database : 'CotizadorTelmexElipgo',
    // port: 3306  //PUERTO IP
    port: 3333
});

const peticion = (sql) => new Promise((resolve, reject) =>{
    connectionDB.query(sql, function(error, results, fields){
        if(error){reject}
        else{resolve(results)}
    })
})  //PETICION PARA CUALQUIER PETICION SQL
 
// Creamos un método en el controlador, en este caso una accion de pruebas
function AgregarVivaIDE(req, res){
    fs.readFile('Documentos/datos.json', 'utf8', function (err,data) {
    if (err) {
        return console.log('test');
    }
    const obj = JSON.parse(data);

    for(var i=3; i<obj['SAC VIVA IDE'].length ; i++){
        const objAgregar = obj['SAC VIVA IDE'][i]
        const sql = `INSERT INTO productoViva(V_Codigo, Texto_Breve_Material, Nota_Interna, Peso_Bruto, 
            Tamaño_Dimesión, Texto_Datos_Básico, Precio_CLIENTE_Renta, Grupo, Carrier, Subgrupo, Tipo, Clase, 
            Línea_de_Producto, Cobertura, Paquete, Cotizar_por_SE, Funcionalidad, SMRZBL, Cuadrante, Proyecto,
            Contratación_o_Renta, Moneda_Base, Tarifa_Bloqueada, Desc_Max, Desc_1_ano, 
            Desc_2_ano, Desc_3_ano, Desc_4_ano, Desc_5_ano, CATEGORIA, FAMILIA, ELEMENTO, PLAZO, 
            idtipoViva, idversionDocumento) values ('${objAgregar.V}','${objAgregar['Texto Breve Material (SAU:Descripción Corta)']}','${objAgregar['Nota Interna (SAU:Descripción Larga)']}','${objAgregar['Peso Bruto (SAU:Ancho Banda)']}',
            '${objAgregar['Tamaño Dimesión (SAU:Mínimo)']}','${objAgregar['Texto de Datos Básico (SAU:Maximo)']}','${objAgregar['Precio CLIENTE Renta    MXN']}','${objAgregar.Grupo}','${objAgregar.Carrier}','${objAgregar.Subgrupo}',
            '${objAgregar.Tipo}','${objAgregar.Clase}','${objAgregar['Línea de Producto']}','${objAgregar.Cobertura}','${objAgregar.Paquete}','${objAgregar['Cotizar por SE']}','${objAgregar.Funcionalidad}','${objAgregar.SMRZBL}',
            '${objAgregar.Cuadrante}','${objAgregar.Proyecto}','${objAgregar['Contratación o Renta']}','${objAgregar['Moneda Base']}','${objAgregar['Tarifa Bloqueada']}','${objAgregar['Desc     (MAXIMO)']}','${objAgregar['Desc     (1 año)']}',
            '${objAgregar['Desc     (2 años)']}','${objAgregar['Desc     (3 años)']}','${objAgregar['Desc     (4 años)']}','${objAgregar['Desc     (5 años)']}',
            '${objAgregar.CATEGORIA}','${objAgregar.FAMILIA}','${objAgregar.ELEMENTO}','${objAgregar.PLAZO}',1,1)`

        connectionDB.query(sql, function(error, results, fields){
            if (error) throw error;
            console.log('The solution is: ', results);
        });
    } //Guardamos los valores de SAC VIVA IDE en un nuevo arreglo
    
    res.send('Los datos VIVA IDE han sido agregados');

    });
}
function AgregarVivaInfinitum(req, res){
    fs.readFile('Documentos/datos.json', 'utf8', function (err,data) {
        if (err) {
            return console.log('test');
        }
        const obj = JSON.parse(data);
    
        for(var i=3; i<obj['SAC VIVA IFM'].length ; i++){
            const objAgregar = obj['SAC VIVA IFM'][i]
            const sql = `INSERT INTO productoViva(V_Codigo, Texto_Breve_Material, Nota_Interna, Peso_Bruto, 
                Tamaño_Dimesión, Texto_Datos_Básico, Precio_CLIENTE_Renta, Grupo, Carrier, Subgrupo, Tipo, Clase, 
                Línea_de_Producto, Cobertura, Paquete, Cotizar_por_SE, Funcionalidad, SMRZBL, Cuadrante, Proyecto,
                Contratación_o_Renta, Moneda_Base, Tarifa_Bloqueada, Desc_Max, Desc_1_ano, 
                Desc_2_ano, Desc_3_ano, Desc_4_ano, Desc_5_ano, CATEGORIA, FAMILIA, ELEMENTO, PLAZO, 
                idtipoViva, idversionDocumento) values ('${objAgregar.V}','${objAgregar['Texto Breve Material (SAU:Descripción Corta)']}','${objAgregar['Nota Interna (SAU:Descripción Larga)']}','${objAgregar['Peso Bruto (SAU:Ancho Banda)']}',
                '${objAgregar['Tamaño Dimesión (SAU:Mínimo)']}','${objAgregar['Texto de Datos Básico (SAU:Maximo)']}','${objAgregar['Precio CLIENTE Renta    MXN']}','${objAgregar.Grupo}','${objAgregar.Carrier}','${objAgregar.Subgrupo}',
                '${objAgregar.Tipo}','${objAgregar.Clase}','${objAgregar['Línea de Producto']}','${objAgregar.Cobertura}','${objAgregar.Paquete}','${objAgregar['Cotizar por SE']}','${objAgregar.Funcionalidad}','${objAgregar.SMRZBL}',
                '${objAgregar.Cuadrante}','${objAgregar.Proyecto}','${objAgregar['Contratación o Renta']}','${objAgregar['Moneda Base']}','${objAgregar['Tarifa Bloqueada']}','${objAgregar['Desc     (MAXIMO)']}','${objAgregar['Desc     (1 año)']}',
                '${objAgregar['Desc     (2 años)']}','${objAgregar['Desc     (3 años)']}','${objAgregar['Desc     (4 años)']}','${objAgregar['Desc     (5 años)']}',
                '${objAgregar.CATEGORIA}','${objAgregar.FAMILIA}','${objAgregar.ELEMENTO}','${objAgregar.PLAZO}',2,1)`
    
            connectionDB.query(sql, function(error, results, fields){
                if (error) throw error;
                console.log('The solution is: ', results);
            });
        } //Guardamos los valores de SAC VIVA IDE en un nuevo arreglo
        
        res.send('Los datos VIVA IFM han sido agregados');
    
        });
}

function ObtenerProductosIDE(req, res){
    //SERVICIO VIVA IDE
    const sql = `select * from productoViva where idtipoViva = 1`;
    connectionDB.query(sql, function(error, results, fields){
        try {
            var datos = results;
            for(let i=0; i<datos.length ; i++){
                if(datos[i].Precio_CLIENTE_Renta === ' $-   '){
                    datos[i].precioInt = 0;
                }else{
                    datos[i].precioInt = parseFloat(datos[i].Precio_CLIENTE_Renta.replace('$', ''))
                }
            }
            res.send(datos)
          } catch (error) {
            console.log(error);
            res.send({ code:400, failed: "error occurred"});
          }
        
    })
    
}
function ObtenerProductosInfinitum(req, res){
    //SERVICIO VIVA Infinitum
    let datos;
    
    const sql = `select * from productoViva where idtipoViva = 2`;
    connectionDB.query(sql, function(error, results, fields){
        try {
            var datos = results;
            for(let i=0; i<datos.length ; i++){
                if(datos[i].Precio_CLIENTE_Renta === ' $-   '){
                    datos[i].precioInt = 0;
                }else{
                    datos[i].precioInt = parseFloat(datos[i].Precio_CLIENTE_Renta.replace('$', ''))
                }
            }
            res.send(datos)
          } catch (error) {
            console.log(error);
            res.send({ code:400, failed: "error occurred"});
          }
    })
    
}
function ObtenerFamiliasIDE(req, res){
    //SERVICIO VIVA IDE
    const sql = `select FAMILIA from productoViva where idtipoViva = 1 group by FAMILIA`;
    connectionDB.query(sql, function(error, results, fields){
        try {
            console.log('Familias')
            res.send(results)
          } catch (error) {
            console.log(error);
            res.send({ code:400, failed: "error occurred"});
          }
    })
    
}
function ObtenerFamiliasInfinitum(req, res){
    //SERVICIO VIVA IDE
    const sql = `select FAMILIA from productoViva where idtipoViva = 2 group by FAMILIA`;
    connectionDB.query(sql, function(error, results, fields){
        try {
            res.send(results)
          } catch (error) {
            console.log(error);
            res.send({ code:400, failed: "error occurred"});
          }
    })
    
}

function CrearDocumento(req, res){
    const Proyecto = req.body;
    const Sucursales = Proyecto.sucursales;


    //Guardamos los datos dentro de la BD
    const sql = `INSERT INTO proyecto (nombreProyecto, numSucursales, statusProyecto) VALUES ('${Proyecto.nombreProyecto}'
                    ,'${Proyecto.numeroSucursales}',1)`
    peticion(sql).then(
        data => {
            const id = data.insertId;
            console.log(id)
            for(let i = 0; i<Sucursales.length; i++){
                const sucursal = Sucursales[i];
                const sql2= `INSERT INTO Sucursales (nombreSucursal, tipoDeServicio, totalSucursal) VALUES ('${sucursal.nombreSucursal}', ${sucursal.tipoServicio}, ${sucursal.Total})`
                peticion(sql2).then(
                    data2 => {
                        const idSucursal = data2.insertId;
                        for(let j=0; j<sucursal.productosRenta.length; j++){
                            //Productos de Renta
                            const producto = sucursal.productosRenta[j]; 
                            const sql3 = `INSERT INTO productosSucursal (idProyecto, idSucursal, idProducto, cantidadProducto, total, tipoDeProducto) VALUES 
                                    (${id}, ${idSucursal},${producto.idproductoViva}, ${producto.Cantidad}, ${producto.Total}, 1)`
                            peticion(sql3).then(
                                data => {
                                    console.log(data)
                                }
                            ).catch( ()=> console.log('valio verga 3'))
                        }
                        res.send(true)
                    }
                ).catch(()=> console.log('valio verga 2'))
            }
        }
    ).catch(()=>{
        console.log("valio verga")
    });
    //Creamos el Documento
    
    var Excel = require('exceljs');
    var workbook = new Excel.Workbook();
    const fileExcel = path.join(__dirname, '../Documentos/Prueba.xlsx');
    // workbook.xlsx.readFile(fileExcel).then(
    //      function(){
    //         //Resumen
    //         const resumenSheet = workbook.getWorksheet(1);
    //         var suma = 0;
    //         for(let i =0; i<Sucursales.length; i++){
    //             const nummeroRow = 6 + i;
    //             const row = resumenSheet.getRow(nummeroRow);
    //             row.getCell(1).value = Sucursales[i].nombreSucursal;
    //             row.getCell(3).value = Sucursales[i].Total;
    //             row.getCell(4).value = Sucursales[i].Total;
    //             suma = suma + Sucursales[i].Total;
    //         }
    //         resumenSheet.getCell('D18').value= suma;
            
    //         //Sucursales
    //         for(let j = 1, m=0; m<Sucursales.length; j++, m++){
    //             const Sucursal = Sucursales[m];
    //             const SucursalSheet = workbook.getWorksheet(`Hoja ${j}`);
    //             SucursalSheet.name = `${Sucursal.nombreSucursal}`;

    //             if(Sucursal.tipoServicio == 0){
    //                 SucursalSheet.getCell('A6').value = 'SERVICIO VIVA INTERNET IDE'
    //             }else{
    //                 SucursalSheet.getCell('A6').value ='SERVICIO VIVA INTERNET INFINITUM'
    //             }
                
    //             SucursalSheet.getCell('A7').value = Sucursal.nombreSucursal;
    //             for(let k=0 ; k<Sucursal.productosRenta.length; k++){
    //                 const producto = Sucursal.productosRenta[k]
    //                 const nummeroRow = 11 + k;
    //                 const row = SucursalSheet.getRow(nummeroRow);
    //                 row.getCell(1).value = producto.V_Codigo;
    //                 row.getCell(2).value = producto.CATEGORIA;
    //                 row.getCell(3).value = producto.FAMILIA;
    //                 row.getCell(4).value = producto.Nota_Interna;
    //                 row.getCell(6).value = producto.Cantidad;
    //                 row.getCell(7).value = producto.precioInt;
    //                 row.getCell(8).value = producto.Total;
    //             }

    //             SucursalSheet.getCell('H34').value = Sucursal.Total;
                
    //             console.log(Sucursal.productoCotizacion)
    //             if(Sucursal.productoCotizacion == undefined){
    //                 Sucursal.productoCotizacion = []
    //             }

    //             for(let t=0; t<Sucursal.productoCotizacion.length; t++){
    //                 const producto = Sucursal.productosRenta[t]
    //                 const nummeroRow = 44 + t;
    //                 const row = SucursalSheet.getRow(nummeroRow);
    //                 row.getCell(1).value = producto.V_Codigo;
    //                 row.getCell(2).value = producto.CATEGORIA;
    //                 row.getCell(3).value = producto.FAMILIA;
    //                 row.getCell(4).value = producto.Nota_Interna;
    //                 row.getCell(6).value = producto.Cantidad;
    //             }
    //         }

    //         //Eliminar las ultimas hojas
    //         const NmSuc = Sucursales.length;
    //         for(let r= NmSuc + 1; r<=20; r++){
    //             workbook.removeWorksheet(`Hoja ${r}`);
    //         }

    //         return workbook.xlsx.writeFile(path.join(__dirname, '../Documentos/new.xlsx'));
    //     }
    // ).then(
    //     () =>{
    //         const file = path.join(__dirname, '../Documentos/new.xlsx');
    //         res.sendFile(file)  
    //     }
    // )



    
}
 
// Exportamos las funciones en un objeto json para poder usarlas en otros fuera de este fichero
module.exports = {
    AgregarVivaIDE,
    AgregarVivaInfinitum,
    ObtenerProductosInfinitum,
    ObtenerProductosIDE,
    ObtenerFamiliasIDE,
    ObtenerFamiliasInfinitum,
    CrearDocumento
};

//Codigooo que mas adelante usaremos
// precio = obj['SAC VIVA IDE'][4]['Precio CLIENTE Renta    MXN']
//     console.log(precio.replace('$', ''))
//     console.log(parseFloat(precio.replace('$', '')))


// for(var i= 0; i<Productos[0].familia.length; i++){
//     const sqlFamilia = `select * from productoViva where idtipoViva = 1 and FAMILIA = "${Productos[0].familia[i].FAMILIA}"`;
//     connectionDB.query(sqlFamilia, function(error,results , fields){
//         if (error) throw error;
//         Productos[0].familia[i]= results
//         console.log(i)
//         if(i==75){
//             res.send(Productos)
//         }
//     })
// }